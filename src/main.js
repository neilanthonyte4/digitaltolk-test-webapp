import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { VueQueryPlugin } from "vue-query";
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import MoonLoader from 'vue-spinner/src/MoonLoader.vue';
import ClipLoader from 'vue-spinner/src/ClipLoader.vue';
import RingLoader from 'vue-spinner/src/RingLoader.vue';

import App from './App.vue'
import router from './router'
/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core';

/* import specific icons */
import {
    faPlus,
    faCalendarDays,
    faClock,
    faCheck,
    faList,
    faCircleInfo,
    faMapLocationDot,
    faMap,
    faRightFromBracket,
    faEnvelope,
    faKey
} from '@fortawesome/free-solid-svg-icons';

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// import DatePicker from 'vue3-date-time-picker';
// import 'vue3-date-time-picker/dist/main.css';

import { DatePicker } from 'v-calendar';
import 'v-calendar/dist/style.css';

/* add icons to the library */
library.add(
    faPlus,
    faCalendarDays,
    faClock,
    faCheck,
    faList,
    faCircleInfo,
    faMapLocationDot,
    faMap,
    faRightFromBracket,
    faEnvelope,
    faKey
)

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(VueQueryPlugin)
app.use(VueToast);

app.component('moon-loader', MoonLoader)
app.component('ring-loader', RingLoader)
app.component('clip-loader', ClipLoader)
app.component('font-awesome-icon', FontAwesomeIcon)
app.component('DatePicker', DatePicker);

app.mount('#app')
