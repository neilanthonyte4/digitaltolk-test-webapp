import server from '../../feathers-client';
import _ from 'underscore';
import {useQueryClient} from 'vue-query';

const todoHooks = () => {
    const queryClient = useQueryClient();

    return {
        fetchTodos: async (props) => {
            try {
                const {queryKey} = props;
                const currentUserId = queryKey[1];
                const todoService = server.service('todos');
                const res = await todoService.find({
                    query: {
                        isDeleted: false,
                        user: currentUserId,
                    }
                });

                return res.data;
            } catch (error) {
                throw new Error(error);
            }
        },
        deleteTodo: async (id) => {
            try {
                const todoService = server.service('todos');
                const onDelete = await todoService.patch(id, {isDeleted: true});
                queryClient.invalidateQueries(['TODO_LIST']);
                return onDelete;
            } catch (error) {
                throw new Error(error);
            }
        },
        updateTodo: async (id, newValue) => {
            try {
                const todoService = server.service('todos');
                const onUpdate = await todoService.patch(id, {current_location: newValue});
                queryClient.invalidateQueries(['TODO_LIST']);
                return onUpdate;
            } catch (error) {
                throw new Error(error);
            }
        },
        startTodo: async (id) => {
            try {
                const todoService = server.service('todos');
                const onStart = await todoService.patch(id, {startedAt: new Date()});
                queryClient.invalidateQueries(['TODO_LIST']);
                return onStart;
            } catch (error) {
                throw error;
            }
        },
    }
}

export default todoHooks;